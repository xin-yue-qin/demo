// vue自带的开发环境下的代理配置，部署上线的时候若是还存在跨域，这个不起作用
// 在proxy中设置跨域,在vue中使用proxy进行跨域的原理是：将域名发送给本地的服务器（启动vue项目的服务,loclahost:8080），
// 再由本地的服务器去请求真正的服务器。
const vueConfig = {
    lintOnSave: false,
    devServer: {
        // development server port 8000
        port: 8000,

        proxy: {
            "/proxy/": {//以/proxy/为开头的适合这个规则
                target:"https://view.inews.qq.com",
                "secure": true,//false为http访问，true为https访问
                "changeOrigin": true,//跨域访问设置，true代表跨域
                "pathRewrite": {//路径改写规则
                    "^/proxy": ""//以/proxy/为开头的改写为''
                    //下面这种也行
                    //  "^/api":"/list"//以/api/为开头的改写为'/list'
                },
                // "headers": {//设置请求头伪装成手机端的访问
                //     "User-Agent": "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Mobile Safari/537.36"
                // }
            },

            "/proxy1/": {//以/proxy/为开头的适合这个规则
                target:"https://c.m.163.com",
                // target: "https://interface.sina.cn",//目标地址
                "secure": true,//false为http访问，true为https访问
                "changeOrigin": true,//跨域访问设置，true代表跨域
                "pathRewrite": {//路径改写规则
                    "^/proxy1": ""//以/proxy/为开头的改写为''
                    //下面这种也行
                    //  "^/api":"/list"//以/api/为开头的改写为'/list'
                },
                // "headers": {//设置请求头伪装成手机端的访问
                //     "User-Agent": "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Mobile Safari/537.36"
                // }
            },
        }
    }
}





module.exports = vueConfig
