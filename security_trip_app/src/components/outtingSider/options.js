import * as echarts from "echarts";

const borderRadius = [15,15, 0, 0]

const date = ["5.12",
  "5.13",
  "5.14",
  "5.15",
  "5.16",
  "5.17",
  "5.18",
  "5.19",
  "5.20",
  "5.21",
  "5.22",
  "5.23",
  "5.24",
  "5.25",];

const count1 = [
  8500,
  4700,
  9000,
  7000,
  1600,
  6400,
  12000,
  3600,
  3000,
  2600,
  6800,
  4200,
  7600,
  5300,
];

const count2 = [
  1000,
  2000,
  2500,
  7000,
  5300,
  5000,
  5800,
  8900,
  1000,
  2400,
  4500,
  6200,
  4100,
  3000,
];

const options1 = {
  color: ["#5470C6"],
  title: {
    text: "各景点实时人群聚集度",
  },
  tooltip: {
    trigger: 'axis',
  },
  legend: {
    data: ["地点"],
  },
  grid: {
    left:'15%',
  },
  xAxis: {
    data: [
      "天安门广场",
      "八达岭长城",
      "颐和园",
      "天坛公园",
      "圆明园",
      "故宫博物院",
    ],
    axisLabel :{
      interval:0,
      fontSize:10,
      rotate:40
    }, 
  },
  yAxis: {},
  dataZoom:{
    show: false,
    startValue:0,
    endValue:5,
  },
  series: [
    {
      name: "人数",
      type: "bar",
      data: [5000, 11000, 3600, 1000, 1000, 2000],
      itemStyle:{
        borderRadius: borderRadius,
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
          {
            offset: 0,
            color: '#5052EE'
          },
          {
            offset: 1,
            color: '#AB6EE5'
          }
        ])
      },
    },
  ],

};

var options2 = {
  color: ["tomato"],
  title: {
    text: "八达岭长城过去两周人群聚集度",
  },
  tooltip: {
    trigger: 'axis',
  },
  legend: {
    data: ["地点"],
  },
  dataZoom:{
    show: false,
    startValue:0,
    endValue:5,
  },
  xAxis: {
    data: date,
    axisLabel :{
      interval:0,
      rotate:40,
      fontSize:10
    },  
  },
  yAxis: {
  },
  series: [
    {
      name: "人数",
      type: "bar",
      data: count1,
      itemStyle:{
        borderRadius: borderRadius,
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
          {
            offset: 0,
            color: '#5052EE'
          },
          {
            offset: 1,
            color: '#AB6EE5'
          }
        ])
      },
    },
  ],
};

var options3 = {
  color: ["tomato"],
  title: {
    text: "天安门广场过去两周人群聚集度",
  },
  tooltip: {},
  legend: {
    data: ["地点"],
  },
  xAxis: {
    data: date,
    axisLabel :{
      interval:0,
      rotate:40,
      fontSize:10
    }, 
  },
  yAxis: {},
  dataZoom:{
    show: false,
    startValue:0,
    endValue:5,
  },
  series: [
    {
      name: "人数",
      type: "bar",
      data: count2,
      itemStyle:{
        borderRadius: borderRadius,
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
          {
            offset: 0,
            color: '#5052EE'
          },
          {
            offset: 1,
            color: '#AB6EE5'
          }
        ])
      },
    },
  ],
};

export default {
  "": options1,
  "八达岭长城": options2,
  "天安门广场": options3
};