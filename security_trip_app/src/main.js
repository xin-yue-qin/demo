import Vue from 'vue'
import App from './App.vue'

import router from './router'
import {VueJsonp} from 'vue-jsonp'
Vue.use(VueJsonp)


//引入组件库
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css';
import './core/lazy_use'
import $ from 'jquery'
import * as echarts from 'echarts'  //5.0以上版本导入方式
Vue.prototype.$echarts = echarts

Vue.config.productionTip = false
Vue.prototype.$ = $;
Vue.use(Antd);

new Vue({
  router,  //注入路由器

  render: h => h(App),
}).$mount('#app')
