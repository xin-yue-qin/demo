import Vue from 'vue'
import VueRouter from 'vue-router'
import TravelHelper from '../views/travelHelper.vue'
//显示安装路由器
Vue.use(VueRouter);

const router = new VueRouter({
    scrollBehavior: () => ({y : 0}),
    routes: [
        {
            path: '/1',
            name: '出行助手',
            component: TravelHelper
        },
        {
            path: '/',
            name: 'epidemicData',
            component: () => import('@/views/epidemic.vue')
        },

    ]
})

// 将路由组件暴露出来
export default router;
